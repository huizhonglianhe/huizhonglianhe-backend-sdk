<?php
namespace Sdk\Backend\Common\View;

use Marmot\Framework\Interfaces\IView;
use Marmot\Framework\View\JsonApiTrait;

use Sdk\Backend\Policy\Model;
use Sdk\Backend\Policy\View;

/**
 * @SuppressWarnings(PHPMD)
 */
abstract class CommonView implements IView
{
    use JsonApiTrait;

    private $rules;

    private $data;
    
    private $encodingParameters;

    public function __construct($data, $encodingParameters = null)
    {
        $this->data = $data;
        $this->encodingParameters = $encodingParameters;

        $this->rules = array(
            Model\Policy::class => View\PolicySchema::class,
            Model\NullPolicy::class =>View\PolicySchema::class,
            Model\Interpretation::class =>View\InterpretationSchema::class,
            Model\NullInterpretation::class =>View\InterpretationSchema::class
        );
    }

    public function display()
    {
        return $this->jsonApiFormat($this->data, $this->rules, $this->encodingParameters);
    }
}
