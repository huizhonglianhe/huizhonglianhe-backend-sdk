<?php
namespace Sdk\Backend\Crew\Model;

use Marmot\Common\Model\IObject;
use Marmot\Common\Model\Object;

class Crew implements IObject
{
    const STATUS = array(
        'ENABLED' => 0 ,
        'DISABLED' => -2
    );

    use Object;

    protected $id;

    protected $cellphone;

    protected $userName;

    protected $password;

    private $realName;

    private $workNumber;

    private $avatar;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->cellphone = '';
        $this->userName = '';
        $this->password = '';
        $this->realName = '';
        $this->workNumber = '';
        $this->avatar = array();
        $this->status = self::STATUS['ENABLED'];
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->statusTime = 0;
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->cellphone);
        unset($this->userName);
        unset($this->password);
        unset($this->realName);
        unset($this->workNumber);
        unset($this->avatar);
        unset($this->status);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setCellphone(string $cellphone) : void
    {
        $this->cellphone = $cellphone;
    }

    public function getCellphone() : string
    {
        return $this->cellphone;
    }

    public function setUserName(string $userName) : void
    {
        $this->userName = $userName;
    }

    public function getUserName() : string
    {
        return $this->userName;
    }

    public function setPassword(string $password) : void
    {
        $this->password = $password;
    }

    public function getPassword() : string
    {
        return $this->password;
    }

    public function setRealName(string $realName) : void
    {
        $this->realName = $realName;
    }

    public function getRealName() : string
    {
        return $this->realName;
    }

    public function setWorkNumber(string $workNumber) : void
    {
        $this->workNumber = $workNumber;
    }

    public function getWorkNumber() : string
    {
        return $this->workNumber;
    }

    public function setAvatar(array $avatar) : void
    {
        $this->avatar = $avatar;
    }

    public function getAvatar() : array
    {
        return $this->avatar;
    }

    public function setStatus(int $status) : void
    {
        $this->status = in_array(
            $status,
            self::STATUS
        ) ? $status : self::STATUS['ENABLED'];
    }
}
