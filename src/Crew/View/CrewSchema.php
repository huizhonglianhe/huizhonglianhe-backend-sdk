<?php
namespace Sdk\Backend\Crew\View;

use Neomerx\JsonApi\Schema\SchemaProvider;

/**
 * @codeCoverageIgnore
 */
class CrewSchema extends SchemaProvider
{
    protected $resourceType = 'crews';

    public function getId($crew) : int
    {
        return $crew->getId();
    }

    public function getAttributes($crew) : array
    {
        return [
            'userName' => $crew->getUserName(),
            'realName' => $crew->getRealName(),
            'cellphone'  => $crew->getCellphone(),
            'workNumber' => $crew->getWorkNumber(),
            'avatar' => $crew->getAvatar(),
            'status' => $crew->getStatus(),
            'createTime' => $crew->getCreateTime(),
            'updateTime' => $crew->getUpdateTime(),
            'statusTime' => $crew->getStatusTime()
        ];
    }
}
