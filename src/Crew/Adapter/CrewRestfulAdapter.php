<?php
namespace Sdk\Backend\Crew\Adapter;

use Sdk\Backend\Crew\Model\NullCrew;
use Sdk\Backend\Crew\Translator\CrewRestfulTranslator;

use Sdk\Common\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

use Marmot\Framework\Interfaces\ITranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

class CrewRestfulAdapter extends GuzzleAdapter implements ICrewAdapter
{
    use AsyncFetchAbleRestfulAdapterTrait, FetchAbleRestfulAdapterTrait;

    private $translator;

    private $resource;

    public function __construct(string $uri, array $authKey)
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new CrewRestfulTranslator();
        $this->scenario = array();
        $this->resource = 'crews';
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    protected function getTranslator() : ITranslator
    {
        return $this->translator;
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullCrew::getInstance());
    }
}
