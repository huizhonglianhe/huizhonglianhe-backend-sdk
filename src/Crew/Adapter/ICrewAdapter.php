<?php
namespace Sdk\Backend\Crew\Adapter;

use Sdk\Common\Common\Adapter\IFetchAbleAdapter;

use Marmot\Framework\Interfaces\IAsyncAdapter;
use Marmot\Framework\Interfaces\IErrorAdapter;

interface ICrewAdapter extends IAsyncAdapter, IFetchAbleAdapter, IErrorAdapter
{
}
