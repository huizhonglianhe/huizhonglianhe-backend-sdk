<?php
namespace Sdk\Backend\Crew\Repository;

use Sdk\Common\Common\Repository\AsyncRepositoryTrait;
use Sdk\Common\Common\Repository\FetchRepositoryTrait;
use Sdk\Common\Common\Repository\ErrorRepositoryTrait;

use Sdk\Backend\Crew\Adapter\ICrewAdapter;
use Sdk\Backend\Crew\Adapter\CrewRestfulAdapter;

class CrewRepository implements ICrewAdapter
{
    use FetchRepositoryTrait, AsyncRepositoryTrait, ErrorRepositoryTrait;

    private $adapter;

    public function __construct(string $uri, array $authKey)
    {
        $this->adapter = new CrewRestfulAdapter(
            $uri,
            $authKey
        );
    }

    public function __destruct()
    {
        unset($this->uri);
        unset($this->authKey);
    }

    protected function getAdapter()
    {
        return $this->adapter;
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
