<?php
namespace Sdk\Backend\Crew\Translator;

use Sdk\Backend\Crew\Model\Crew;
use Sdk\Backend\Crew\Model\NullCrew;

use Sdk\Common\Common\Translator\RestfulTranslatorTrait;

use Marmot\Framework\Interfaces\ITranslator;

class CrewRestfulTranslator implements ITranslator
{
    use RestfulTranslatorTrait;

    public function arrayToObject(array $expression, $object = null)
    {
        return $this->translateToObject($expression, $object);
    }

    public function objectToArray($object, array $keys = array())
    {
        unset($object);
        unset($keys);
        return [];
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $crew = null)
    {
        if (empty($expression)) {
            return new NullCrew();
        }

        if ($crew == null) {
            $crew = new Crew();
        }

        $data =  $expression['data'];

        $id = $data['id'];
        $crew->setId($id);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['cellphone'])) {
            $crew->setCellphone($attributes['cellphone']);
        }
        if (isset($attributes['userName'])) {
             $crew->setUserName($attributes['userName']);
        }
        if (isset($attributes['realName'])) {
            $crew->setRealName($attributes['realName']);
        }
        if (isset($attributes['password'])) {
            $crew->setPassword($attributes['password']);
        }
        if (isset($attributes['workNumber'])) {
            $crew->setWorkNumber($attributes['workNumber']);
        }
        if (isset($attributes['avatar'])) {
            $crew->setAvatar($attributes['avatar']);
        }
        if (isset($attributes['statusTime'])) {
            $crew->setStatusTime($attributes['statusTime']);
        }
        if (isset($attributes['createTime'])) {
            $crew->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $crew->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['status'])) {
            $crew->setStatus($attributes['status']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();
        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        return $crew;
    }
}
