<?php
namespace Sdk\Backend;

use Marmot\Framework\Interfaces\ISdk;

use Sdk\Backend\Policy\Adapter\IPolicyAdapter;
use Sdk\Backend\Policy\Adapter\IInterpretationAdapter;

use Sdk\Backend\Policy\Repository\PolicyRepository;
use Sdk\Backend\Policy\Repository\InterpretationRepository;

use Sdk\Backend\Crew\Adapter\ICrewAdapter;
use Sdk\Backend\Crew\Repository\CrewRepository;

class Sdk implements ISdk
{
    private $uri;

    private $authKey;

    private $policyRepository;

    private $interpretationRepository;

    private $crewRepository;

    public function __construct(string $uri, array $authKey)
    {
        $this->uri = $uri;
        $this->authKey = $authKey;
        $this->policyRepository = new PolicyRepository($uri, $authKey);
        $this->interpretationRepository = new InterpretationRepository($uri, $authKey);
        $this->crewRepository = new CrewRepository($uri, $authKey);
    }

    public function __destruct()
    {
        unset($this->uri);
        unset($this->authKey);
        unset($this->policyRepository);
        unset($this->interpretationRepository);
        unset($this->crewRepository);
    }

    public function getUri() : string
    {
        return $this->uri;
    }

    public function getAuthKey() : array
    {
        return $this->authKey;
    }

    public function policyRepository() : IPolicyAdapter
    {
        return $this->policyRepository;
    }

    public function interpretationRepository() : IInterpretationAdapter
    {
        return $this->interpretationRepository;
    }

    public function crewRepository() : ICrewAdapter
    {
        return $this->crewRepository;
    }
}
