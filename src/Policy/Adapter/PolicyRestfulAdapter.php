<?php
namespace Sdk\Backend\Policy\Adapter;

use Sdk\Backend\Policy\Translator\PolicyRestfulTranslator;
use Sdk\Backend\Policy\Model\NullPolicy;

use Sdk\Common\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Sdk\Common\Common\Adapter\FetchAbleRestfulAdapterTrait;

use Marmot\Framework\Adapter\Restful\GuzzleAdapter;
use Marmot\Framework\Interfaces\ITranslator;

class PolicyRestfulAdapter extends GuzzleAdapter implements IPolicyAdapter
{
    use AsyncFetchAbleRestfulAdapterTrait, FetchAbleRestfulAdapterTrait;

    private $translator;

    private $resource;

    public function __construct(string $uri, array $authKey)
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new PolicyRestfulTranslator();
        $this->scenario = array();
        $this->resource = 'policies';
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    protected function getTranslator() : ITranslator
    {
        return $this->translator;
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullPolicy::getInstance());
    }
}
