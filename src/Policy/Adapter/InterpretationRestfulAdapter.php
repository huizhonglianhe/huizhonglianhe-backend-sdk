<?php
namespace Sdk\Backend\Policy\Adapter;

use Sdk\Backend\Policy\Translator\InterpretationRestfulTranslator;
use Sdk\Backend\Policy\Model\NullInterpretation;

use Sdk\Common\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Sdk\Common\Common\Adapter\FetchAbleRestfulAdapterTrait;

use Marmot\Framework\Adapter\Restful\GuzzleAdapter;
use Marmot\Framework\Interfaces\ITranslator;

class InterpretationRestfulAdapter extends GuzzleAdapter implements IInterpretationAdapter
{
    use AsyncFetchAbleRestfulAdapterTrait, FetchAbleRestfulAdapterTrait;

    private $translator;

    private $resource;

    public function __construct(string $uri, array $authKey)
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new InterpretationRestfulTranslator();
        $this->scenario = array();
        $this->resource = 'policyInterpretations';
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    protected function getTranslator() : ITranslator
    {
        return $this->translator;
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullInterpretation::getInstance());
    }
}
