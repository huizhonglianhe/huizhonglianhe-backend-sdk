<?php
namespace Sdk\Backend\Policy\Adapter;

use Sdk\Common\Common\Adapter\IFetchAbleAdapter;

use Marmot\Framework\Interfaces\IAsyncAdapter;
use Marmot\Framework\Interfaces\IErrorAdapter;

interface IPolicyAdapter extends IAsyncAdapter, IFetchAbleAdapter, IErrorAdapter
{
}
