<?php
namespace Sdk\Backend\Policy\Translator;

use Sdk\Backend\Policy\Model\Interpretation;
use Sdk\Backend\Policy\Model\NullInterpretation;

use Sdk\Common\Common\Translator\RestfulTranslatorTrait;

use Marmot\Framework\Interfaces\ITranslator;

class InterpretationRestfulTranslator implements ITranslator
{
    use RestfulTranslatorTrait;

    protected function getPolicyRestfulTranslator() : PolicyRestfulTranslator
    {
        return new PolicyRestfulTranslator();
    }
    
    public function arrayToObject(array $expression, $object = null)
    {
        return $this->translateToObject($expression, $object);
    }

    public function objectToArray($object, array $keys = array())
    {
        unset($object);
        unset($keys);
        return [];
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $interpretation = null)
    {
        if (empty($expression)) {
            return new NullInterpretation();
        }

        if ($interpretation == null) {
            $interpretation = new Interpretation();
        }

        $data =  $expression['data'];

        $id = $data['id'];
        $interpretation->setId($id);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['cover'])) {
            $interpretation->setCover($attributes['cover']);
        }
        if (isset($attributes['title'])) {
             $interpretation->setTitle($attributes['title']);
        }
        if (isset($attributes['detail'])) {
            $interpretation->setDetail($attributes['detail']);
        }
        if (isset($attributes['description'])) {
            $interpretation->setDescription($attributes['description']);
        }
        if (isset($attributes['status'])) {
            $interpretation->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $interpretation->setStatusTime($attributes['statusTime']);
        }
        if (isset($attributes['createTime'])) {
            $interpretation->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $interpretation->setUpdateTime($attributes['updateTime']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['policy']['data'])) {
            $policy = $this->changeArrayFormat($relationships['policy']['data']);
            $interpretation->setPolicy($this->getPolicyRestfulTranslator()->arrayToObject($policy));
        }

        return $interpretation;
    }
}
