<?php
namespace Sdk\Backend\Policy\Translator;

use Sdk\Backend\Policy\Model\Policy;
use Sdk\Backend\Policy\Model\NullPolicy;

use Sdk\Common\Common\Translator\RestfulTranslatorTrait;

use Marmot\Framework\Interfaces\ITranslator;

class PolicyRestfulTranslator implements ITranslator
{
    use RestfulTranslatorTrait;

    public function arrayToObject(array $expression, $object = null)
    {
        return $this->translateToObject($expression, $object);
    }

    public function objectToArray($object, array $keys = array())
    {
        unset($object);
        unset($keys);
        return [];
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $policy = null)
    {
        if (empty($expression)) {
            return new NullPolicy();
        }

        if ($policy == null) {
            $policy = new Policy();
        }

        $data =  $expression['data'];

        $id = $data['id'];
        $policy->setId($id);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['number'])) {
            $policy->setNumber($attributes['number']);
        }
        if (isset($attributes['title'])) {
             $policy->setTitle($attributes['title']);
        }
        if (isset($attributes['applicableObject'])) {
            $policy->setApplicableObject($attributes['applicableObject']);
        }
        if (isset($attributes['dispatchDepartment'])) {
            $policy->setDispatchDepartment($attributes['dispatchDepartment']);
        }
        if (isset($attributes['applicableIndustries'])) {
            $policy->setApplicableIndustries($attributes['applicableIndustries']);
        }
        if (isset($attributes['level'])) {
            $policy->setLevel($attributes['level']);
        }
        if (isset($attributes['classify'])) {
            $policy->setClassify($attributes['classify']);
        }
        if (isset($attributes['detail'])) {
            $policy->setDetail($attributes['detail']);
        }
        if (isset($attributes['description'])) {
            $policy->setDescription($attributes['description']);
        }
        if (isset($attributes['statusTime'])) {
            $policy->setStatusTime($attributes['statusTime']);
        }
        if (isset($attributes['createTime'])) {
            $policy->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $policy->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['status'])) {
            $policy->setStatus($attributes['status']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();
        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        return $policy;
    }
}
