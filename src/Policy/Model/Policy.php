<?php
namespace Sdk\Backend\Policy\Model;

use Marmot\Common\Model\IObject;
use Marmot\Common\Model\Object;

use Marmot\Core;

class Policy implements IObject
{
    const STATUS = array(
        'NORMAL' => 0 ,
        'DELETED' => -2
    );

    use Object;

    /**
     * @var string $title 标题
     */
    private $title;
    /**
     * @var string $number 编号
     */
    private $number;
    /**
     * @var array $applicableObject 适用对象
     */
    private $applicableObject;
    /**
     * @var array $dispatchDepartment 发文部门
     */
    private $dispatchDepartment;
    /**
     * @var array $applicableIndustries 适用行业
     */
    private $applicableIndustries;
    /**
     * @var int $level 政策级别
     */
    private $level;
    /**
     * @var array $classify 政策分类
     */
    private $classify;
    /**
     * @var array $detail 详情
     */
    private $detail;
    /**
     * @var string $description 描述
     */
    private $description;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->title = '';
        $this->number = '';
        $this->applicableObject = array();
        $this->dispatchDepartment = array();
        $this->applicableIndustries = array();
        $this->level = 0;
        $this->classify = array();
        $this->detail = array();
        $this->description = '';
        $this->status = self::STATUS['NORMAL'];
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->statusTime = 0;
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->title);
        unset($this->number);
        unset($this->applicableObject);
        unset($this->dispatchDepartment);
        unset($this->applicableIndustries);
        unset($this->level);
        unset($this->classify);
        unset($this->detail);
        unset($this->description);
        unset($this->status);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setTitle(string $title) : void
    {
        $this->title = $title;
    }

    public function getTitle() : string
    {
        return $this->title;
    }

    public function setNumber(string $number) : void
    {
        $this->number = $number;
    }

    public function getNumber() : string
    {
        return $this->number;
    }

    public function setApplicableObject(array $applicableObject) : void
    {
        $this->applicableObject = $applicableObject;
    }

    public function getApplicableObject() : array
    {
        return $this->applicableObject;
    }

    public function setDispatchDepartment(array $dispatchDepartment) : void
    {
        $this->dispatchDepartment = $dispatchDepartment;
    }

    public function getDispatchDepartment() : array
    {
        return $this->dispatchDepartment;
    }

    public function setApplicableIndustries(array $applicableIndustries) : void
    {
        $this->applicableIndustries = $applicableIndustries;
    }

    public function getApplicableIndustries() : array
    {
        return $this->applicableIndustries;
    }

    public function setLevel(int $level) : void
    {
        $this->level = $level;
    }

    public function getLevel() : int
    {
        return $this->level;
    }

    public function setClassify(array $classify) : void
    {
        $this->classify = $classify;
    }

    public function getClassify() : array
    {
        return $this->classify;
    }

    public function setDetail(array $detail) : void
    {
        $this->detail = $detail;
    }

    public function getDetail() : array
    {
        return $this->detail;
    }

    public function setDescription(string $description) : void
    {
        $this->description = $description;
    }

    public function getDescription() : string
    {
        return $this->description;
    }

    public function setStatus(int $status) : void
    {
        $this->status = in_array(
            $status,
            self::STATUS
        ) ? $status : self::STATUS['NORMAL'];
    }

    public function isNormal() : bool
    {
        return $this->getStatus() == self::STATUS['NORMAL'];
    }
}
