<?php
namespace Sdk\Backend\Policy\Model;

use Marmot\Core;
use Marmot\Framework\Interfaces\INull;

class NullPolicy extends Policy implements INull
{
    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
