<?php
namespace Sdk\Backend\Policy\Model;

use Marmot\Framework\Interfaces\INull;

trait BindInterpretationTrait
{
    public function bindInterpretation(Interpretation $interpretation) : bool
    {
        if ($this->isValidateInterpretation($interpretation)) {
            return $this->bindInterpretatioAction($interpretation);
        }

        return false;
    }

    public function unBindInterpretation(Interpretation $interpretation) : bool
    {
        if ($this->isInterpretationExist($interpretation)) {
            return $this->unBindInterpretatioAction($interpretation);
            ;
        }
        return false;
    }

    protected function isValidateInterpretation(Interpretation $interpretation)
    {
        return $this->isInterpretationStatusNormal($interpretation) &&
               $this->isInterpretationExist($interpretation);
    }

    private function isInterpretationStatusNormal(Interpretation $interpretation) : bool
    {
        if (!$interpretation->isNormal()) {
            $this->bindInterpretationStatusIsNotNormal();
            return false;
        }

        return true;
    }

    private function isInterpretationExist(Interpretation $interpretation) : bool
    {
        if ($interpretation instanceof INull) {
            $this->bindInterpretationStatusIsNotExist();
            return false;
        }
        return true;
    }

    /**
     * 实现该性状需要实现自己的绑定方法
     */
    abstract protected function bindInterpretatioAction(Interpretation $interpretation) : bool;
    /**
     * 实现该性状需要实现自己的解绑方法
     */
    abstract protected function unBindInterpretatioAction(Interpretation $interpretation) : bool;

    /**
     * 如果政策解释状态不正确, 错误触发函数
     */
    abstract protected function bindInterpretationStatusIsNotNormal() : void;
    /**
     * 如果政策解释不存在, 错误触发函数
     */
    abstract protected function bindInterpretationStatusIsNotExist() : void;
}
