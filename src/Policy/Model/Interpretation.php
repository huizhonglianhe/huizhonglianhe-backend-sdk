<?php
namespace Sdk\Backend\Policy\Model;

use Marmot\Common\Model\IObject;
use Marmot\Common\Model\Object;

use Marmot\Core;

class Interpretation implements IObject
{
    use Object;

    /**
     * @var string $title 标题
     */
    private $title;
    /**
     * @var array $detail 详情
     */
    private $detail;
    /**
     * @var array $cover 封面
     */
    private $cover;
    /**
     * @var string $description 描述
     */
    private $description;
    /**
     * @var Policy $policy 政策
     */
    private $policy;

    const STATUS = array(
        'NORMAL' => 0 ,
        'DELETED' => -2
    );

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->title = '';
        $this->detail = array();
        $this->cover = array();
        $this->description = '';
        $this->policy = new Policy();
        $this->status = self::STATUS['NORMAL'];
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->statusTime = 0;
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->title);
        unset($this->detail);
        unset($this->cover);
        unset($this->description);
        unset($this->status);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setTitle(string $title) : void
    {
        $this->title = $title;
    }

    public function getTitle() : string
    {
        return $this->title;
    }

    public function setCover(array $cover) : void
    {
        $this->cover = $cover;
    }

    public function setDetail(array $detail) : void
    {
        $this->detail = $detail;
    }

    public function getDetail() : array
    {
        return $this->detail;
    }

    public function getCover() : array
    {
        return $this->cover;
    }

    public function setDescription(string $description) : void
    {
        $this->description = $description;
    }

    public function getDescription() : string
    {
        return $this->description;
    }

    public function setPolicy(Policy $policy) : void
    {
        $this->policy = $policy;
    }

    public function getPolicy() : Policy
    {
        return $this->policy;
    }

    public function setStatus(int $status) : void
    {
        $this->status = in_array(
            $status,
            self::STATUS
        ) ? $status : self::STATUS['NORMAL'];
    }

    public function isNormal() : bool
    {
        return $this->getStatus() == self::STATUS['NORMAL'];
    }
}
