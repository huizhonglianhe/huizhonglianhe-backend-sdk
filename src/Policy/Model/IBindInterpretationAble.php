<?php
namespace Sdk\Backend\Policy\Model;

interface IBindInterpretationAble
{
    public function bindInterpretation(Interpretation $interpretation) : bool;

    public function unBindInterpretation(Interpretation $interpretation) : bool;
}
