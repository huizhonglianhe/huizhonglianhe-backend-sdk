<?php
namespace Sdk\Backend\Policy\View;

use Neomerx\JsonApi\Schema\SchemaProvider;

/**
 * @codeCoverageIgnore
 */
class PolicySchema extends SchemaProvider
{
    protected $resourceType = 'policies';

    public function getId($policy) : int
    {
        return $policy->getId();
    }

    public function getAttributes($policy) : array
    {
        return [
            'number' => $policy->getNumber(),
            'title' => $policy->getTitle(),
            'applicableObject'  => $policy->getApplicableObject(),
            'dispatchDepartment'  => $policy->getDispatchDepartment(),
            'applicableIndustries'  => $policy->getApplicableIndustries(),
            'level'  => $policy->getLevel(),
            'classify'  => $policy->getClassify(),
            'detail' => $policy->getDetail(),
            'description' => $policy->getDescription(),
            'status' => $policy->getStatus(),
            'createTime' => $policy->getCreateTime(),
            'updateTime' => $policy->getUpdateTime(),
            'statusTime' => $policy->getStatusTime()
        ];
    }
}
