<?php
namespace Sdk\Backend\Policy\View;

use Neomerx\JsonApi\Schema\SchemaProvider;

/**
 * @codeCoverageIgnore
 */
class InterpretationSchema extends SchemaProvider
{
    protected $resourceType = 'policyInterpretations';

    public function getId($interpretation) : int
    {
        return $interpretation->getId();
    }

    public function getAttributes($interpretation) : array
    {
        return [
            'title' => $interpretation->getTitle(),
            'detail' => $interpretation->getDetail(),
            'cover'  => $interpretation->getCover(),
            'description' => $interpretation->getDescription(),
            'status' => $interpretation->getStatus(),
            'createTime' => $interpretation->getCreateTime(),
            'updateTime' => $interpretation->getUpdateTime(),
            'statusTime' => $interpretation->getStatusTime()
        ];
    }

    public function getRelationships($interpretation, $isPrimary, array $includeList)
    {
        unset($isPrimary);
        unset($includeList);
        return [
            'policy' => [self::DATA => $interpretation->getPolicy()]
        ];
    }
}
