<?php
namespace Sdk\Backend\Policy\Repository;

use Sdk\Common\Common\Repository\AsyncRepositoryTrait;
use Sdk\Common\Common\Repository\FetchRepositoryTrait;
use Sdk\Common\Common\Repository\ErrorRepositoryTrait;

use Sdk\Backend\Policy\Adapter\IInterpretationAdapter;
use Sdk\Backend\Policy\Adapter\InterpretationRestfulAdapter;

class InterpretationRepository implements IInterpretationAdapter
{
    use FetchRepositoryTrait, AsyncRepositoryTrait, ErrorRepositoryTrait;

    private $adapter;

    public function __construct(string $uri, array $authKey)
    {
        $this->adapter = new InterpretationRestfulAdapter(
            $uri,
            $authKey
        );
    }

    public function __destruct()
    {
        unset($this->uri);
        unset($this->authKey);
    }

    protected function getAdapter()
    {
        return $this->adapter;
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
