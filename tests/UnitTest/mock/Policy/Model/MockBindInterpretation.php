<?php
namespace Sdk\Backend\Policy\Model;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

class MockBindInterpretation implements IBindInterpretationAble
{
    use BindInterpretationTrait;

    protected function bindInterpretatioAction(Interpretation $interpretation) : bool
    {
        return false;
    }

    protected function unBindInterpretatioAction(Interpretation $interpretation) : bool
    {
        return false;
    }

    protected function bindInterpretationStatusIsNotNormal() : void
    {
    }

    protected function bindInterpretationStatusIsNotExist() : void
    {
    }

    public function publicIsValidateInterpretation(Interpretation $interpretation)
    {
        return $this->isValidateInterpretation($interpretation);
    }
}
