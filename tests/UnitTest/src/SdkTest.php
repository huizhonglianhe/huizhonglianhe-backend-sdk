<?php
namespace Sdk\Backend;

use Marmot\Framework\Interfaces\ISdk;

use PHPUnit\Framework\TestCase;

class SdkTest extends TestCase
{
    private $stub;

    private $uri = 'uri';

    private $authKey = ['apikey'=>'xxxx'];

    public function setUp()
    {
        $this->stub = new Sdk($this->uri, $this->authKey);
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testGetUri()
    {
        $this->assertEquals($this->uri, $this->stub->getUri());
    }

    public function testGetAuthKey()
    {
        $this->assertEquals($this->authKey, $this->stub->getAuthKey());
    }

    public function testPolicyRepository()
    {
        $this->assertInstanceOf('Sdk\Backend\Policy\Adapter\IPolicyAdapter', $this->stub->policyRepository());
    }

    public function testInterpretationRepository()
    {
        $this->assertInstanceOf(
            'Sdk\Backend\Policy\Repository\InterpretationRepository',
            $this->stub->interpretationRepository()
        );
    }
}
