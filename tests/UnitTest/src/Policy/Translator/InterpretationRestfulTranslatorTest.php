<?php
namespace Sdk\Backend\Policy\Translator;

use Sdk\Backend\Policy\Model\Interpretation;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

class InterpretationRestfulTranslatorTest extends TestCase
{
    private $stub;

    private $childStub;

    public function setUp()
    {
        parent::setUp();
        $this->stub = new InterpretationRestfulTranslator();
        $this->childStub = new Class extends InterpretationRestfulTranslator {
            public function getPolicyRestfulTranslator() : PolicyRestfulTranslator
            {
                return parent::getPolicyRestfulTranslator();
            }
        };
    }

    public function tearDown()
    {
        parent::tearDown();
        unset($this->stub);
        unset($this->childStub);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Interfaces\ITranslator',
            $this->stub
        );
    }

    public function testArrayToObjectIncorrectObject()
    {
        $result = $this->stub->arrayToObject(array(), new Interpretation());
        $this->assertInstanceOf('Sdk\Backend\Policy\Model\NullInterpretation', $result);
    }

    public function testObjectToArray()
    {
        $result = $this->stub->objectToArray(new Interpretation(), array());
        $this->assertEquals([], $result);
    }


    public function testGetPolicyRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Backend\Policy\Translator\PolicyRestfulTranslator',
            $this->childStub->getPolicyRestfulTranslator()
        );
    }
}
