<?php
namespace Sdk\Backend\Policy\Translator;

use Sdk\Backend\Policy\Model\Policy;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

class PolicyRestfulTranslatorTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        parent::setUp();
        $this->translator = new PolicyRestfulTranslator();
    }

    public function tearDown()
    {
        parent::tearDown();
        unset($this->translator);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Interfaces\ITranslator',
            $this->translator
        );
    }

    public function testArrayToObjectIncorrectObject()
    {
        $result = $this->translator->arrayToObject(array(), new Policy());
        $this->assertInstanceOf('Sdk\Backend\Policy\Model\NullPolicy', $result);
    }

    public function testObjectToArray()
    {
        $result = $this->translator->objectToArray(new Policy(), array());
        $this->assertEquals([], $result);
    }
}
