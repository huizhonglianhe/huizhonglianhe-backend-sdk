<?php
namespace Sdk\Backend\Policy\Repository;

use Sdk\Backend\Policy\Adapter\PolicyRestfulAdapter;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

class PolicyRepositoryTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(PolicyRepository::class)
            ->setMethods(['getAdapter'])
            ->setConstructorArgs(
                [
                    'uri'=>'',
                    'authKey'=>[]
                ]
            )
            ->getMock();

        $this->childStub = new class('', []) extends PolicyRepository {
            public function getAdapter() : PolicyRestfulAdapter
            {
                return parent::getAdapter();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testGetAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Backend\Policy\Adapter\PolicyRestfulAdapter',
            $this->childStub->getAdapter()
        );
    }
    
    public function testScenario()
    {
        $adapter = $this->prophesize(PolicyRestfulAdapter::class);
        $adapter->scenario(
            Argument::exact('scenario')
        )->shouldBeCalledTimes(1);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());
        $result = $this->stub->scenario('scenario');
        $this->assertEquals($this->stub, $result);
    }
}
