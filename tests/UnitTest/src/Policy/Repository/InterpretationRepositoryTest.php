<?php
namespace Sdk\Backend\Policy\Repository;

use Sdk\Backend\Policy\Adapter\InterpretationRestfulAdapter;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

class InterpretationRepositoryTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(InterpretationRepository::class)
            ->setMethods(['getAdapter'])
            ->setConstructorArgs(
                [
                    'uri'=>'',
                    'authKey'=>[]
                ]
            )
            ->getMock();

        $this->childStub = new class('', []) extends InterpretationRepository {
            public function getAdapter() : InterpretationRestfulAdapter
            {
                return parent::getAdapter();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testGetAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Backend\Policy\Adapter\InterpretationRestfulAdapter',
            $this->childStub->getAdapter()
        );
    }
    
    public function testScenario()
    {
        $adapter = $this->prophesize(InterpretationRestfulAdapter::class);
        $adapter->scenario(
            Argument::exact('scenario')
        )->shouldBeCalledTimes(1);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());
        $result = $this->stub->scenario('scenario');
        $this->assertEquals($this->stub, $result);
    }
}
