<?php
namespace Sdk\Backend\Policy\Model;

use Marmot\Framework\Interfaces\INull;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

class BindInterpretationTraitTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockBindInterpretation::class)->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testImplementsIBindInterpretationAble()
    {
        $this->assertInstanceOf(
            'Sdk\Backend\Policy\Model\IBindInterpretationAble',
            $this->stub
        );
    }

    public function testBindInterpretationSuccess()
    {
        $interpretation = new Interpretation();
        $this->stub = $this->getMockBuilder(MockBindInterpretation::class)
                            ->setMethods(
                                [
                                    'isValidateInterpretation',
                                    'bindInterpretatioAction'
                                ]
                            )->getMock();

        $this->stub->expects($this->once())
                   ->method('isValidateInterpretation')
                   ->willReturn(true);

        $this->stub->expects($this->once())
                   ->method('bindInterpretatioAction')
                    ->willReturn(true);

        $result = $this->stub->bindInterpretation($interpretation);
        $this->assertTrue($result);
    }

    public function testUnBindInterpretationSuccess()
    {
        $interpretation = new Interpretation();
        $this->stub = $this->getMockBuilder(MockBindInterpretation::class)
                            ->setMethods(
                                [
                                    'unBindInterpretatioAction'
                                ]
                            )->getMock();


        $this->stub->expects($this->once())
                   ->method('unBindInterpretatioAction')
                    ->willReturn(true);

        $result = $this->stub->unBindInterpretation($interpretation);
        $this->assertTrue($result);
    }

    public function testBindInterpretationValidateFalse()
    {
        $interpretation = new Interpretation();
        $this->stub = $this->getMockBuilder(MockBindInterpretation::class)
                            ->setMethods(
                                [
                                    'isValidateInterpretation',
                                    'bindInterpretatioAction'
                                ]
                            )->getMock();

        $this->stub->expects($this->once())
                   ->method('isValidateInterpretation')
                   ->willReturn(false);

        $this->stub->expects($this->exactly(0))
                   ->method('bindInterpretatioAction');

        $result = $this->stub->bindInterpretation($interpretation);
        $this->assertFalse($result);
    }

    public function testUnBindInterpretationNotExistFalse()
    {
        $interpretation = new NullInterpretation();
        $this->stub = $this->getMockBuilder(MockBindInterpretation::class)
                            ->setMethods(
                                [
                                    'unBindInterpretatioAction'
                                ]
                            )->getMock();


        $this->stub->expects($this->exactly(0))
                   ->method('unBindInterpretatioAction');

        $result = $this->stub->unBindInterpretation($interpretation);
        $this->assertFalse($result);
    }

    public function testBindInterpretationActionFalse()
    {
        $interpretation = new Interpretation();
        $this->stub = $this->getMockBuilder(MockBindInterpretation::class)
                            ->setMethods(
                                [
                                    'isValidateInterpretation',
                                    'bindInterpretatioAction'
                                ]
                            )->getMock();

        $this->stub->expects($this->once())
                   ->method('isValidateInterpretation')
                   ->willReturn(true);

        $this->stub->expects($this->exactly(1))
                   ->method('bindInterpretatioAction')
                   ->willReturn(false);

        $result = $this->stub->bindInterpretation($interpretation);
        $this->assertFalse($result);
    }

    public function testUnBindInterpretationActionFalse()
    {
        $interpretation = new Interpretation();
        $this->stub = $this->getMockBuilder(MockBindInterpretation::class)
                            ->setMethods(
                                [
                                    'unBindInterpretatioAction'
                                ]
                            )->getMock();


        $this->stub->expects($this->once())
                   ->method('unBindInterpretatioAction')
                   ->willReturn(false);

        $result = $this->stub->unBindInterpretation($interpretation);
        $this->assertFalse($result);
    }

    public function testIsValidateInterpretationStatusIsNotNormal()
    {
        $this->stub = $this->getMockBuilder(MockBindInterpretation::class)
                            ->setMethods(
                                [
                                    'bindInterpretationStatusIsNotNormal'
                                ]
                            )->getMock();

        $interpretation = new Interpretation();
        $interpretation->setStatus(Interpretation::STATUS['DELETED']);

        $this->stub->expects($this->once())
                   ->method('bindInterpretationStatusIsNotNormal');

        $result = $this->stub->publicIsValidateInterpretation($interpretation);
        $this->assertFalse($result);
    }

    public function testIsValidateInterpretationStatusIsNormal()
    {
        $interpretation = new Interpretation();
        $interpretation->setStatus(Interpretation::STATUS['NORMAL']);

        $this->stub = new MockBindInterpretation();
        $result = $this->stub->publicIsValidateInterpretation($interpretation);
        $this->assertTrue($result);
    }

    public function testIsValidateInterpretationNotExist()
    {
        $this->stub = $this->getMockBuilder(MockBindInterpretation::class)
                            ->setMethods(
                                [
                                    'bindInterpretationStatusIsNotExist'
                                ]
                            )->getMock();

        $interpretation = new NullInterpretation();

        $this->stub->expects($this->once())
                   ->method('bindInterpretationStatusIsNotExist');

        $result = $this->stub->publicIsValidateInterpretation($interpretation);
        $this->assertFalse($result);
    }
}
