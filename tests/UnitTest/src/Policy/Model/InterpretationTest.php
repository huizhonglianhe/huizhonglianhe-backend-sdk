<?php
namespace Sdk\Backend\Policy\Model;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

class InterpretationTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new Interpretation();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testCorrectImplementsIObject()
    {
        $this->assertInstanceof('Marmot\Common\Model\IObject', $this->stub);
    }

    public function testConstructor()
    {
        $this->assertEquals(0, $this->stub->getId());
        $this->assertEquals('', $this->stub->getTitle());
        $this->assertEquals([], $this->stub->getDetail());
        $this->assertEquals([], $this->stub->getCover());
        $this->assertEquals('', $this->stub->getDescription());
        $this->assertEquals(0, $this->stub->getCreateTime());
        $this->assertEquals(0, $this->stub->getUpdateTime());
        $this->assertEquals(0, $this->stub->getStatusTime());
        $this->assertInstanceof('Sdk\Backend\Policy\Model\Policy', $this->stub->getPolicy());
        $this->assertEquals(Policy::STATUS['NORMAL'], $this->stub->getStatus());
    }

    //id 测试 --------------------------------------------------- start
    public function testSetIdCorrectType()
    {
        $this->stub->setId(1);
        $this->assertEquals(1, $this->stub->getId());
    }
    //id 测试 ---------------------------------------------------   end

    //title
    public function testSetTitleCorrectType()
    {
        $this->stub->setTitle('title');
        $this->assertEquals('title', $this->stub->getTitle());
    }
    /**
     * @expectedException TypeError
     */
    public function testSetNameWrongType()
    {
        $this->stub->setTitle([]);
    }

    //detail
    public function testSetDetailCorrectType()
    {
        $this->stub->setDetail(['test']);
        $this->assertEquals(['test'], $this->stub->getDetail());
    }
    /**
     * @expectedException TypeError
     */
    public function testSetDetailWrongType()
    {
        $this->stub->setDetail('');
    }

    //cover
    public function testSetCoverCorrectType()
    {
        $this->stub->setCover(['test']);
        $this->assertEquals(['test'], $this->stub->getCover());
    }
    /**
     * @expectedException TypeError
     */
    public function testSetCoverWrongType()
    {
        $this->stub->setCover('');
    }

    //description
    public function testSetDescriptionCorrectType()
    {
        $this->stub->setDescription('description');
        $this->assertEquals('description', $this->stub->getDescription());
    }
    /**
     * @expectedException TypeError
     */
    public function testSetDescriptionWrongType()
    {
        $this->stub->setDescription([]);
    }

    //policy
    public function testSetPolicyCorrectType()
    {
        $policy = new Policy();
        $this->stub->setPolicy($policy);
        $this->assertEquals($policy, $this->stub->getPolicy());
    }
    /**
     * @expectedException TypeError
     */
    public function testSetPolicyWrongType()
    {
        $this->stub->setPolicy('');
    }

    //status
    /**
     * @dataProvider statusProvider
     */
    public function testSetStatus($actual, $expected)
    {
        $this->stub->setStatus($actual);
        $this->assertEquals($expected, $this->stub->getStatus());
    }
    public function statusProvider()
    {
        return [
            [Policy::STATUS['NORMAL'], Policy::STATUS['NORMAL']],
            [Policy::STATUS['DELETED'], Policy::STATUS['DELETED']],
            [999, Policy::STATUS['NORMAL']]
        ];
    }
    public function testIsNormal()
    {
        $this->stub->setStatus(Policy::STATUS['DELETED']);
        $result = $this->stub->isNormal();
        $this->assertFalse($result);
    }
}
