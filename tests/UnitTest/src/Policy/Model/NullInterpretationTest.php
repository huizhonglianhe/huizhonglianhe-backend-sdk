<?php
namespace Sdk\Backend\Policy\Model;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class NullInterpretationTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = NullInterpretation::getInstance();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testExtendsMember()
    {
        $this->assertInstanceof('Sdk\Backend\Policy\Model\Interpretation', $this->stub);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Framework\Interfaces\INull', $this->stub);
    }
}
