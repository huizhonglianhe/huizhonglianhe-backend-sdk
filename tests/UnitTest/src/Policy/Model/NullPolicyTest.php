<?php
namespace Sdk\Backend\Policy\Model;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class NullPolicyTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = NullPolicy::getInstance();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testExtendsMember()
    {
        $this->assertInstanceof('Sdk\Backend\Policy\Model\Policy', $this->stub);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Framework\Interfaces\INull', $this->stub);
    }
}
