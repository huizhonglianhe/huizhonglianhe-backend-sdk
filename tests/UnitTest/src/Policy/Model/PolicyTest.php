<?php
namespace Sdk\Backend\Policy\Model;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

class PolicyTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new Policy();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testCorrectImplementsIObject()
    {
        $this->assertInstanceof('Marmot\Common\Model\IObject', $this->stub);
    }

    public function testConstructor()
    {
        $this->assertEquals(0, $this->stub->getId());
        $this->assertEquals('', $this->stub->getTitle());
        $this->assertEquals('', $this->stub->getNumber());
        $this->assertEquals([], $this->stub->getApplicableObject());
        $this->assertEquals([], $this->stub->getDispatchDepartment());
        $this->assertEquals([], $this->stub->getApplicableIndustries());
        $this->assertEquals([], $this->stub->getClassify());
        $this->assertEquals([], $this->stub->getDetail());
        $this->assertEquals(0, $this->stub->getLevel());
        $this->assertEquals('', $this->stub->getDescription());
        $this->assertEquals(0, $this->stub->getCreateTime());
        $this->assertEquals(0, $this->stub->getUpdateTime());
        $this->assertEquals(0, $this->stub->getStatusTime());
        $this->assertEquals(Policy::STATUS['NORMAL'], $this->stub->getStatus());
    }

    //id 测试 --------------------------------------------------- start
    public function testSetIdCorrectType()
    {
        $this->stub->setId(1);
        $this->assertEquals(1, $this->stub->getId());
    }
    //id 测试 ---------------------------------------------------   end

    //title
    public function testSetTitleCorrectType()
    {
        $this->stub->setTitle('title');
        $this->assertEquals('title', $this->stub->getTitle());
    }
    /**
     * @expectedException TypeError
     */
    public function testSetNameWrongType()
    {
        $this->stub->setTitle([]);
    }

    //number
    public function testSetNumberCorrectType()
    {
        $this->stub->setNumber('number');
        $this->assertEquals('number', $this->stub->getNumber());
    }
    /**
     * @expectedException TypeError
     */
    public function testSetNumberWrongType()
    {
        $this->stub->setNumber([]);
    }

    //applicableObject
    public function testSetApplicableObjectCorrectType()
    {
        $this->stub->setApplicableObject(['test']);
        $this->assertEquals(['test'], $this->stub->getApplicableObject());
    }
    /**
     * 设置 Policy setApplicableObject() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetApplicableObjectWrongType()
    {
        $this->stub->setApplicableObject('');
    }

    //dispatchDepartment
    public function testSetDispatchDepartmentCorrectType()
    {
        $this->stub->setDispatchDepartment(['test']);
        $this->assertEquals(['test'], $this->stub->getDispatchDepartment());
    }
    /**
     * 设置 Policy setDispatchDepartment() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDispatchDepartmentWrongType()
    {
        $this->stub->setDispatchDepartment('');
    }

    //applicableIndustries
    public function testSetApplicableIndustriesCorrectType()
    {
        $this->stub->setApplicableIndustries(['test']);
        $this->assertEquals(['test'], $this->stub->getApplicableIndustries());
    }
    /**
     * 设置 Policy setApplicableIndustries() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetApplicableIndustriesWrongType()
    {
        $this->stub->setApplicableIndustries('');
    }

    //level
    public function testSetLevel()
    {
        $this->stub->setLevel(1);
        $this->assertEquals(1, $this->stub->getLevel());
    }

    //classify
    public function testSetClassifyCorrectType()
    {
        $this->stub->setClassify(['test']);
        $this->assertEquals(['test'], $this->stub->getClassify());
    }
    /**
     * 设置 Policy setClassify() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetClassifyWrongType()
    {
        $this->stub->setClassify('');
    }

    //detail
    public function testSetDetailCorrectType()
    {
        $this->stub->setDetail(['test']);
        $this->assertEquals(['test'], $this->stub->getDetail());
    }
    /**
     * 设置 Policy setDetail() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDetailWrongType()
    {
        $this->stub->setDetail('');
    }

    //description
    public function testSetDescriptionCorrectType()
    {
        $this->stub->setDescription('description');
        $this->assertEquals('description', $this->stub->getDescription());
    }
    /**
     * @expectedException TypeError
     */
    public function testSetDescriptionWrongType()
    {
        $this->stub->setDescription([]);
    }

    //status
    /**
     * @dataProvider statusProvider
     */
    public function testSetStatus($actual, $expected)
    {
        $this->stub->setStatus($actual);
        $this->assertEquals($expected, $this->stub->getStatus());
    }
    public function statusProvider()
    {
        return [
            [Policy::STATUS['NORMAL'], Policy::STATUS['NORMAL']],
            [Policy::STATUS['DELETED'], Policy::STATUS['DELETED']],
            [999, Policy::STATUS['NORMAL']]
        ];
    }
    public function testIsNormal()
    {
        $this->stub->setStatus(Policy::STATUS['DELETED']);
        $result = $this->stub->isNormal();
        $this->assertFalse($result);
    }
}
