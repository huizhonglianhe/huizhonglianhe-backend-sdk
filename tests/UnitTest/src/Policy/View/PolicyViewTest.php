<?php
namespace Sdk\Backend\Policy\View;

use PHPUnit\Framework\TestCase;

class PolicyViewTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new PolicyView([]);
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testExtendsCommView()
    {
        $this->assertInstanceOf('Sdk\Backend\Common\View\CommonView', $this->stub);
    }
}
