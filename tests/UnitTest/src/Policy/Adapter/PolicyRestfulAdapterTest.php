<?php
namespace Sdk\Backend\Policy\Adapter;

use Sdk\Backend\Policy\Model\Policy;
use Sdk\Backend\Policy\Model\NullPolicy;

use Marmot\Framework\Interfaces\ITranslator;
use PHPUnit\Framework\TestCase;

use Prophecy\Argument;

class PolicyRestfulAdapterTest extends TestCase
{
    private $stub;

    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(PolicyRestfulAdapter::class)
            ->setMethods(
                ['fetchOneAction']
            )
            ->setConstructorArgs(array('uri'=>'', 'authKey'=>[]))
            ->getMock();

        $this->childStub = new class('', []) extends PolicyRestfulAdapter
        {
            public function getResource() : string
            {
                return parent::getResource();
            }

            public function getTranslator() : ITranslator
            {
                return parent::getTranslator();
            }

            public function getScenario() : array
            {
                return parent::getScenario();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testImplementsIPolicyAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Backend\Policy\Adapter\IPolicyAdapter',
            $this->stub
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('policies', $this->childStub->getResource());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Backend\Policy\Translator\PolicyRestfulTranslator',
            $this->childStub->getTranslator()
        );
    }

    public function testFetchOne()
    {
        $id = 1;
        $expected = new Policy();

        $this->stub->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, new NullPolicy())
            ->willReturn($expected);

        $result = $this->stub->fetchOne($id);
        $this->assertEquals($expected, $result);
    }
}
