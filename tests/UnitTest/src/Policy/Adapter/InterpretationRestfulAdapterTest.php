<?php
namespace Sdk\Backend\Policy\Adapter;

use Sdk\Backend\Policy\Model\Interpretation;
use Sdk\Backend\Policy\Model\NullInterpretation;

use Marmot\Framework\Interfaces\ITranslator;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

class InterpretationRestfulAdapterTest extends TestCase
{
    private $stub;

    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(InterpretationRestfulAdapter::class)
            ->setMethods(
                ['fetchOneAction']
            )
            ->setConstructorArgs(array('uri'=>'', 'authKey'=>[]))
            ->getMock();

        $this->childStub = new class('', []) extends InterpretationRestfulAdapter
        {
            public function getResource() : string
            {
                return parent::getResource();
            }

            public function getTranslator() : ITranslator
            {
                return parent::getTranslator();
            }

            public function getScenario() : array
            {
                return parent::getScenario();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testImplementsIPolicyAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Backend\Policy\Adapter\IInterpretationAdapter',
            $this->stub
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('policyInterpretations', $this->childStub->getResource());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Backend\Policy\Translator\InterpretationRestfulTranslator',
            $this->childStub->getTranslator()
        );
    }

    public function testFetchOne()
    {
        $id = 1;
        $expected = new Interpretation();

        $this->stub->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, new NullInterpretation())
            ->willReturn($expected);

        $result = $this->stub->fetchOne($id);
        $this->assertEquals($expected, $result);
    }
}
