<?php
namespace Sdk\Backend\Crew\Translator;

use Sdk\Backend\Crew\Model\Crew;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

class CrewRestfulTranslatorTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        parent::setUp();
        $this->translator = new CrewRestfulTranslator();
    }

    public function tearDown()
    {
        parent::tearDown();
        unset($this->translator);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Interfaces\ITranslator',
            $this->translator
        );
    }

    public function testArrayToObjectIncorrectObject()
    {
        $result = $this->translator->arrayToObject(array(), new Crew());
        $this->assertInstanceOf('Sdk\Backend\Crew\Model\NullCrew', $result);
    }

    public function testObjectToArray()
    {
        $result = $this->translator->objectToArray(new Crew(), array());
        $this->assertEquals([], $result);
    }
}
