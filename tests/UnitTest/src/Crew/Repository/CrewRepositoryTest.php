<?php
namespace Sdk\Backend\Crew\Repository;

use Sdk\Backend\Crew\Adapter\CrewRestfulAdapter;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

class CrewRepositoryTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(CrewRepository::class)
            ->setMethods(['getAdapter'])
            ->setConstructorArgs(
                [
                    'uri'=>'',
                    'authKey'=>[]
                ]
            )
            ->getMock();

        $this->childStub = new class('', []) extends CrewRepository {
            public function getAdapter() : CrewRestfulAdapter
            {
                return parent::getAdapter();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testGetAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Backend\Crew\Adapter\CrewRestfulAdapter',
            $this->childStub->getAdapter()
        );
    }
    
    public function testScenario()
    {
        $adapter = $this->prophesize(CrewRestfulAdapter::class);
        $adapter->scenario(
            Argument::exact('scenario')
        )->shouldBeCalledTimes(1);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());
        $result = $this->stub->scenario('scenario');
        $this->assertEquals($this->stub, $result);
    }
}
