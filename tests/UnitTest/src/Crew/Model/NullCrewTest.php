<?php
namespace Sdk\Backend\Crew\Model;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class NullCrewTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = NullCrew::getInstance();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testExtendsMember()
    {
        $this->assertInstanceof('Sdk\Backend\Crew\Model\Crew', $this->stub);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Framework\Interfaces\INull', $this->stub);
    }
}
