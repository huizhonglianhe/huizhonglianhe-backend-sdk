<?php
namespace Sdk\Backend\Crew\Model;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

class CrewTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new Crew();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testCorrectImplementsIObject()
    {
        $this->assertInstanceof('Marmot\Common\Model\IObject', $this->stub);
    }

    public function testConstructor()
    {
        $this->assertEquals(0, $this->stub->getId());
        $this->assertEquals('', $this->stub->getCellphone());
        $this->assertEquals('', $this->stub->getUserName());
        $this->assertEquals('', $this->stub->getPassword());
        $this->assertEquals('', $this->stub->getRealName());
        $this->assertEquals('', $this->stub->getWorkNumber());
        $this->assertEquals([], $this->stub->getAvatar());
        $this->assertEquals(0, $this->stub->getCreateTime());
        $this->assertEquals(0, $this->stub->getUpdateTime());
        $this->assertEquals(0, $this->stub->getStatusTime());
        $this->assertEquals(Crew::STATUS['ENABLED'], $this->stub->getStatus());
    }

    //id 测试 --------------------------------------------------- start
    public function testSetIdCorrectType()
    {
        $this->stub->setId(1);
        $this->assertEquals(1, $this->stub->getId());
    }
    //id 测试 ---------------------------------------------------   end

    //cellphone
    public function testSetTitleCorrectType()
    {
        $this->stub->setCellphone('cellphone');
        $this->assertEquals('cellphone', $this->stub->getCellphone());
    }
    /**
     * @expectedException TypeError
     */
    public function testSetCellphoneWrongType()
    {
        $this->stub->setCellphone(array('cellphone'));
    }

    //userName
    public function testSetUserNameCorrectType()
    {
        $this->stub->setUserName('userName');
        $this->assertEquals('userName', $this->stub->getUserName());
    }
    /**
     * @expectedException TypeError
     */
    public function testSetUserNameWrongType()
    {
        $this->stub->setUserName(array('userName'));
    }

    //realName
    public function testSetRealNameCorrectType()
    {
        $this->stub->setRealName('realName');
        $this->assertEquals('realName', $this->stub->getRealName());
    }
    /**
     * @expectedException TypeError
     */
    public function testSetRealNameWrongType()
    {
        $this->stub->setRealName(array('realName'));
    }

    //password
    public function testSetPasswordCorrectType()
    {
        $this->stub->setPassword('password');
        $this->assertEquals('password', $this->stub->getPassword());
    }
    /**
     * @expectedException TypeError
     */
    public function testSetPasswordWrongType()
    {
        $this->stub->setPassword(array('password'));
    }
    
    //workNumber
    public function testSetWorkNumberCorrectType()
    {
        $this->stub->setWorkNumber('workNumber');
        $this->assertEquals('workNumber', $this->stub->getWorkNumber());
    }
    /**
     * @expectedException TypeError
     */
    public function testSetWorkNumberWrongType()
    {
        $this->stub->setWorkNumber(array('workNumber'));
    }
    
    //avatar
    public function testSetAvatarCorrectType()
    {
        $this->stub->setAvatar(array('avatar'));
        $this->assertEquals(array('avatar'), $this->stub->getAvatar());
    }
    /**
     * @expectedException TypeError
     */
    public function testSetAvatarWrongType()
    {
        $this->stub->setAvatar('avatar');
    }

    //status
    /**
     * @dataProvider statusProvider
     */
    public function testSetStatus($actual, $expected)
    {
        $this->stub->setStatus($actual);
        $this->assertEquals($expected, $this->stub->getStatus());
    }
    public function statusProvider()
    {
        return [
            [Crew::STATUS['ENABLED'], Crew::STATUS['ENABLED']],
            [Crew::STATUS['DISABLED'], Crew::STATUS['DISABLED']],
            [999, Crew::STATUS['ENABLED']]
        ];
    }
}
