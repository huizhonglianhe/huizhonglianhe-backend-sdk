<?php
namespace Sdk\Backend\Crew\Adapter;

use Sdk\Backend\Crew\Model\Crew;
use Sdk\Backend\Crew\Model\NullCrew;

use PHPUnit\Framework\TestCase;
use Marmot\Framework\Interfaces\ITranslator;

class CrewRestfulAdapterTest extends TestCase
{
    private $stub;

    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(CrewRestfulAdapter::class)
            ->setMethods(
                ['fetchOneAction']
            )
            ->setConstructorArgs(array('uri'=>'', 'authKey'=>[]))
            ->getMock();

        $this->childStub = new class('', []) extends CrewRestfulAdapter
        {
            public function getResource() : string
            {
                return parent::getResource();
            }

            public function getTranslator() : ITranslator
            {
                return parent::getTranslator();
            }

            public function getScenario() : array
            {
                return parent::getScenario();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testImplementsICrewAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Backend\Crew\Adapter\ICrewAdapter',
            $this->stub
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('crews', $this->childStub->getResource());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Backend\Crew\Translator\CrewRestfulTranslator',
            $this->childStub->getTranslator()
        );
    }

    public function testFetchOne()
    {
        $id = 1;
        $expected = new Crew();

        $this->stub->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, new NullCrew())
            ->willReturn($expected);

        $result = $this->stub->fetchOne($id);
        $this->assertEquals($expected, $result);
    }
}
