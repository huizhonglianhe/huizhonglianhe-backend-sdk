# 政策解读绑定

## 概述

编写**interface**和**trait**实现通用政策绑定功能.

## 使用说明

**IBindInterpretationAble**有政策绑定能接口

```php
use Sdk\Backend\Policy\Model\IBindInterpretationAble;
use Sdk\Backend\Policy\Model\BindInterpretationTrait;

class xxx implements IBindInterpretationAble
{
  use BindInterpretationTrait;
  
   ...
}
```

使用**性状**, 必须实现2个方法.

### 1. 具体绑定政策操作

`bindInterpretatioAction(Interpretation $interpretation):  bool`

使用模板模式调度判断算法, 这里传递具体政策解说, 内部实现自己绑定工作.

### 2. 具体解除绑定政策操作

`unBindInterpretatioAction(Interpretation $interpretation):  bool`

使用模板模式调度判断算法, 这里传递具体政策解说, 内部实现自己解除绑定工作.

### 3. 错误触发: 政策解说不是正常状态(删除状态)

`bindInterpretationStatusIsNotNormal() : void`

如果政策状态不正确, 会触发`bindInterpretationStatusIsNotNormal()`该方法, 内部实现自己的错误处理机制.

### 3. 错误触发: 政策解说不存在

`bindInterpretationStatusIsNotExist() : void`

如果政策状态不存在, 会触发`bindInterpretationStatusIsNotExist()`该方法, 内部实现自己的错误处理机制.